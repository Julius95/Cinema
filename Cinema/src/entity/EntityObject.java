package entity;

import cinema.DAO_Main_Controller;

public abstract class EntityObject {
	public void persist() {
		DAO_Main_Controller.em.persist(this);
	}

	public void delete() {
		DAO_Main_Controller.em.remove(this);
	}

	public abstract void chainDelete();
}
