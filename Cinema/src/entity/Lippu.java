package entity;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import cinema.DAO_Main_Controller;

@Entity
@Table(name="Lippu")
public class Lippu extends EntityObject implements Serializable{

	private int lippuID;
	private Asiakas asiakas;
	private Paikka paikka;
	private N�yt�s n�yt�s;

	public Lippu() {}

	public Lippu(Paikka paikka, N�yt�s n�yt�s) {
		this.paikka = paikka;
		this.n�yt�s = n�yt�s;
	}

	public Lippu(Asiakas asiakas, Paikka paikka, N�yt�s n�yt�s) {
		this.asiakas = asiakas;
		this.paikka = paikka;
		this.n�yt�s = n�yt�s;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getLippuID() {
		return lippuID;
	}

	public void setLippuID(int lippuID) {
		this.lippuID = lippuID;
	}
	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name="AsiakasNumero")
	public Asiakas getAsiakas() {
		return asiakas;
	}

	public void setAsiakas(Asiakas asiakas) {
		this.asiakas = asiakas;
	}
	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name="PaikkaID")
	public Paikka getPaikka() {
		return paikka;
	}

	public void setPaikka(Paikka paikka) {
		this.paikka = paikka;
	}

	@ManyToOne(cascade = CascadeType.PERSIST)//@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name="N�yt�sID")
	public N�yt�s getN�yt�s() {
		return n�yt�s;
	}

	public void setN�yt�s(N�yt�s n�yt�s) {
		this.n�yt�s = n�yt�s;
	}

	@Override
	public String toString(){
		String el = asiakas == null ? "" : "\nAsiakas : " + asiakas.getEtunimi() + " " + asiakas.getSukunimi();
		String sa = paikka == null ? "" : "\nPaikka : " + paikka.getPaikkaNmr() + " " + paikka.getRiviNmr() + " ID : " + paikka.getPaikkaID();
		return "N�yt�s : " + n�yt�s.getNaytosID() + " \nPvm : " + n�yt�s.getEsitysaika() +
				el + sa + "\nLippu ID : " + lippuID;

	}

	@Override
	public void chainDelete() {

	}


}
