package entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import org.eclipse.persistence.jpa.jpql.parser.DateTime;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import cinema.DAO_Main_Controller;

@Entity
@Table(name="N�yt�s")
public class N�yt�s extends EntityObject implements Serializable{
	private int naytosID;
	private Timestamp esitysaika;
	private Sali sali;
	private Elokuva elokuva;
	private float hinta;

	public N�yt�s() {}

	public N�yt�s(Timestamp esitysaika, float hinta, Sali sali, Elokuva elokuva) {
		this.esitysaika = esitysaika;
		this.hinta = hinta;
		this.sali = sali;
		this.elokuva = elokuva;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getNaytosID() {
		return naytosID;
	}

	public void setNaytosID(int naytosID) {
		this.naytosID = naytosID;
	}
	@Column(name = "Pvm")
	public Timestamp getEsitysaika() {
		return esitysaika;
	}

	public void setEsitysaika(Timestamp esitysaika) {
		this.esitysaika = esitysaika;
	}

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name="SaliID")
	public Sali getSali() {
		return sali;
	}

	public void setSali(Sali sali) {
		this.sali = sali;
	}
	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name="ElokuvaID")
	//@OnDelete(action = OnDeleteAction.CASCADE)
	public Elokuva getElokuva() {
		return elokuva;
	}

	public void setElokuva(Elokuva elokuva) {
		this.elokuva = elokuva;
	}

	@Column
	public float getHinta() {
		return hinta;
	}

	public void setHinta(float hinta) {
		this.hinta = hinta;
	}

	@Override
	public String toString(){
		String el = elokuva == null ? "" : "\nElokuva : " + elokuva.getNimi();
		String sa = sali == null ? "" : "\nSali : " + sali.getSaliID() + " Nimi : " + sali.getNimi();
		return "N�yt�s : " + naytosID + " \nPvm : " + esitysaika.toString() +
				el + sa;

	}

	@Override
	public void chainDelete() {
		String lause = "SELECT li FROM Lippu li WHERE li.n�yt�s = :id";
		Query kysely = DAO_Main_Controller.em.createQuery(lause);
		kysely.setParameter("id", this);
		List<Lippu> liput = kysely.getResultList();
		liput.stream().forEach((li) -> {
			DAO_Main_Controller.em.remove(li);
		});
	}

}
