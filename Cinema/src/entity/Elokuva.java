package entity;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import javax.persistence.*;

import cinema.DAO_Main_Controller;

@Entity
@Table(name="Elokuva")
public class Elokuva extends EntityObject implements Serializable{
	private String nimi;
	private int elokuvaID;
	private float arvosana;
	private Date valmistuspvm;

	public Elokuva(){}

	//Konstruktori jolla asetetaan vain NOT NULL arvot
	public Elokuva(String nimi) {
		this.nimi = nimi;
	}

	public Elokuva(String nimi, float arvosana, Date valmistuspvm) {
		this.nimi = nimi;
		this.arvosana = arvosana;
		this.valmistuspvm = valmistuspvm;
	}

	@Column
	public String getNimi() {
		return nimi;
	}

	public void setNimi(String nimi) {
		this.nimi = nimi;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getElokuvaID() {
		return elokuvaID;
	}

	public void setElokuvaID(int elokuvaID) {
		this.elokuvaID = elokuvaID;
	}

	@Column
	public float getArvosana() {
		return arvosana;
	}

	public void setArvosana(float arvosana) {
		this.arvosana = arvosana;
	}

	@Column(name="ValmistusVuosi")
	public Date getValmistuspvm() {
		return valmistuspvm;
	}

	public void setValmistuspvm(Date valmistuspvm) {
		this.valmistuspvm = valmistuspvm;
	}

	@Override
	public void chainDelete() {
		String lause = "SELECT na FROM N�yt�s na WHERE na.elokuva = :id";
		Query kysely = DAO_Main_Controller.em.createQuery(lause);
		kysely.setParameter("id", this);
		List<N�yt�s> n�yt�kset = kysely.getResultList();
		n�yt�kset.stream().forEach((na) -> {
			na.chainDelete();
			DAO_Main_Controller.em.remove(na);
		});
	}



}
