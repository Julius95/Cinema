package entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import cinema.DAO_Main_Controller;

@Entity
@Table(name = "Sali")
public class Sali extends EntityObject implements Serializable{

	private int saliID;
	private String nimi;
	private List<Paikka> paikat;

	public Sali() {
	}

	public Sali(String nimi) {
		paikat = new ArrayList<Paikka>();
		this.nimi = nimi;
	}

	@Column
	public String getNimi() {
		return nimi;
	}

	public void setNimi(String nimi) {
		this.nimi = nimi;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getSaliID() {
		return saliID;
	}

	public void setSaliID(int saliID) {
		this.saliID = saliID;
	}
	//, orphanRemoval=true

	@OneToMany(mappedBy = "sali", cascade = CascadeType.PERSIST)//,
	//@OrderBy("rivinmr")
	public List<Paikka> getPaikat() {
		return paikat;
	}

	public void setPaikat(List<Paikka> paikat) {
		this.paikat = paikat;
	}

	public void lisaaPaikka(Paikka p) {
		paikat.add(p);
		p.setSali(this);
	}

	@Override
	public void chainDelete() {
		String lause = "SELECT na FROM N�yt�s na WHERE na.sali = :id";
		Query kysely = DAO_Main_Controller.em.createQuery(lause);
		kysely.setParameter("id", this);
		List<N�yt�s> n�yt�kset = kysely.getResultList();
		n�yt�kset.stream().forEach((na) -> {
			na.chainDelete();
			DAO_Main_Controller.em.remove(na);
		});

		lause = "SELECT pa FROM Paikka pa WHERE pa.sali = :id";
		kysely = DAO_Main_Controller.em.createQuery(lause);
		kysely.setParameter("id", this);
		List<Paikka> paikat = kysely.getResultList();
		paikat.stream().forEach((li) -> {
			DAO_Main_Controller.em.remove(li);
		});

	}

	public void haePaikat(){
		paikat.clear();
		String lause = "SELECT pa FROM Paikka pa WHERE pa.sali = :id";
		Query kysely = DAO_Main_Controller.em.createQuery(lause);
		kysely.setParameter("id", this);
		List<Paikka> paikat = kysely.getResultList();
		paikat.stream().forEach((pa) -> {
			this.paikat.add(pa);
		});
	}

}
