package entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import cinema.DAO_Main_Controller;

@Entity
@Table(name="Paikka")
public class Paikka extends EntityObject implements Serializable, Comparable<Paikka>{
	/**
	 *
	 */
	private static final long serialVersionUID = -4865348090959143198L;
	private int paikkaID;
	private int paikkaNmr;
	private int riviNmr;
	private Sali sali;

	public Paikka(){}

	public Paikka(int riviNmr, int paikkaNmr, Sali sali) {
		this.riviNmr = riviNmr;
		this.paikkaNmr = paikkaNmr;
		this.sali = sali;
		//rivi.lisaaPaikka(this);
		sali.lisaaPaikka(this);
	}
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getPaikkaID() {
		return paikkaID;
	}

	public void setPaikkaID(int paikkaID) {
		this.paikkaID = paikkaID;
	}

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name="SaliID")
	public Sali getSali() {
		return sali;
	}

	public void setSali(Sali sali) {
		this.sali = sali;
	}

	@Column(name="paikkanmr")
	public int getPaikkaNmr() {
		return paikkaNmr;
	}

	public void setPaikkaNmr(int paikkaNmr) {
		this.paikkaNmr = paikkaNmr;
	}

	@Column(name="rivinmr")
	public int getRiviNmr() {
		return riviNmr;
	}

	public void setRiviNmr(int riviNmr) {
		this.riviNmr = riviNmr;
	}

	@Override
	public void chainDelete() {
		String lause = "SELECT li FROM Lippu li WHERE li.paikka = :id";
		Query kysely = DAO_Main_Controller.em.createQuery(lause);
		kysely.setParameter("id", this);
		List<Lippu> liput = kysely.getResultList();
		liput.stream().forEach((li) -> {
			DAO_Main_Controller.em.remove(li);
		});
	}

	@Override
	public int compareTo(Paikka arg0) {
		if(riviNmr <= arg0.riviNmr){

			if(riviNmr == arg0.riviNmr){
				if(paikkaNmr < arg0.paikkaNmr){
					return -1;
				}
				return 1;
			}
			return -1;
		}
		return 1;
	}

}
