package entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import cinema.DAO_Main_Controller;

@Entity
@Table(name="Asiakas")//, uniqueConstraints={@UniqueConstraint(columnNames = {"email"})}
public class Asiakas extends EntityObject implements Serializable{
	private int asiakasID;
	private String etunimi, sukunimi, email;

	public Asiakas(){}

	public Asiakas(String enimi, String snimi, String email){
		etunimi = enimi;
		sukunimi = snimi;
		this.email = email;
	}
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getAsiakasID() {
		return asiakasID;
	}

	public void setAsiakasID(int asiakasID) {
		this.asiakasID = asiakasID;
	}
	@Column
	public String getEtunimi() {
		return etunimi;
	}

	public void setEtunimi(String etunimi) {
		this.etunimi = etunimi;
	}

	@Column
	public String getSukunimi() {
		return sukunimi;
	}

	public void setSukunimi(String sukunimi) {
		this.sukunimi = sukunimi;
	}
	@Column(name="Sähköposti", unique=true)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public void chainDelete() {
		String lause = "SELECT li FROM Lippu li WHERE li.asiakas = :id";
		Query kysely = DAO_Main_Controller.em.createQuery(lause);
		kysely.setParameter("id", this);
		List<Lippu> liput = kysely.getResultList();
		liput.stream().forEach((li) -> {
			DAO_Main_Controller.em.remove(li);
		});
	}
}
