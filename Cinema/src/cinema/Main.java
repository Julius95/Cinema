package cinema;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import entity.Asiakas;
import entity.Elokuva;
import entity.Lippu;
import entity.N�yt�s;
import entity.Paikka;
import entity.Sali;
//https://stackoverflow.com/questions/8589805/load-entity-from-view-in-jpa-hibernate
//https://stackoverflow.com/questions/17708946/jpa-native-query-select-and-cast-object
public class Main {

	private static Scanner s = new Scanner(System.in);

	private static DAO_Main_Controller dao = new DAO_Main_Controller("Cinema");

	public static void main(String[] args) {

		int valinta = 0;
		do{
			System.out.println("1. Elokuva valikko"
							+ "\n2. Sali valikko"
							+ "\n3. N�yt�s valikko"
							+ "\n4. Asiakas valikko"
							+ "\n5. Lippu valikko"
							+ "\n7. Luo dummy dataa"
							+ "\n9. Lopeta");
			valinta = s.nextInt();
			switch(valinta){
			case 1:
				movieMenu();
				break;
			case 2:
				saliMenu();
				break;
			case 3:
				n�yt�sMenu();
				break;
			case 4:
				asiakasMenu();
				break;
			case 5:
				lippuMenu();
				break;
			case 7:
				dao.createDummyData();
				System.out.println("Datan luonti valmis...");
				break;
			}
		}while(valinta != 9);

//		dao.createDummyData();

//		dao.createHalls();


	}

	private static void lippuMenu() {
		Lippu li = null;
		N�yt�s na = null;
		Paikka pa = null;
		Asiakas as = null;

		int smenu = 0;
		char c;
		do{
			System.out.println(li == null ? "Ei lippua k�sittelyss�" : "K�sitelt�v� lippu : " + li.toString());
			System.out.println("1.Luo uusi Lippu"
			+ "\n2.Hae Lippu\n3.Poista Lippu\n5.<-Takaisin");
			smenu = s.nextInt();
			switch(smenu){
				case 1:
					System.out.println("Anna n�yt�ksen id");
					na = (N�yt�s) dao.haeEntity(N�yt�s.class, s.nextInt());
					if(na==null){
						System.out.println("N�yt�st� annetulla id:ll� ei l�ytynyt!");
						break;
					}
					na.getSali().haePaikat();
					List<Integer> vapaatIDt = dao.haeVapaatLiput(na.getNaytosID());

					//Tulostetaan Salin paikat tietylle n�yt�kselle
					int viimeisinRivi=1;
					int vapaat = 0;
					System.out.println("-------");
					System.out.println("Sali : " + na.getSali().getNimi() + "\nElokuva : " + na.getElokuva().getNimi() );

					Collections.sort(na.getSali().getPaikat(), new Comparator<Paikka>(){
						@Override
						public int compare(Paikka p1, Paikka p2){
							return p1.compareTo(p2);
						}
					});

					for(Paikka p : na.getSali().getPaikat()){
						//System.out.println(p.getPaikkaNmr() + " "  + p.getRiviNmr());
						if(viimeisinRivi!=p.getRiviNmr())
							System.out.println();
						if(vapaatIDt.contains(p.getPaikkaID())){
							System.out.print("o(" + p.getPaikkaNmr() + " " + p.getRiviNmr() + ") ");
							vapaat++;
						}else{
							System.out.print("x(" + p.getPaikkaNmr() + " " + p.getRiviNmr() + ") ");
						}
						viimeisinRivi = p.getRiviNmr();
					}
					System.out.println("\n-------");

					//Jos ei yht��n paikkaa vapaana
					if(vapaat==0){
						System.out.println("Ei paikkoja vapaana...");
						break;
					}

					pa = null;
					do{
						System.out.println("Anna paikan paikka numero : ");
						int pnmr = s.nextInt();
						System.out.println("Anna paikan rivi : ");
						int rivi = s.nextInt();
						System.out.println(rivi + " " + pnmr);
						for(Paikka p : na.getSali().getPaikat()){
							if(p.getRiviNmr() == rivi && p.getPaikkaNmr() == pnmr){
								System.out.println("Paikka (" + p.getPaikkaNmr() + " " + p.getRiviNmr() + ")");
								//Tarkistetaan, ett� paikan id on vapaiden paikkojen joukossa
								for(Integer i : vapaatIDt){
									if(p.getPaikkaID() == i){
										pa = p;
										System.out.println("Valittu Paikka (" + p.getPaikkaNmr() + " " + p.getRiviNmr() + ")");
										break;
									}
								}
							}
						}
						if(pa == null)
							System.out.println("Annetut koordinaatit v��ri� tai paikka on jo varattu!");
					}while(pa==null);
					s.nextLine();
					System.out.println("Haetaanko asiakas(k/e)? ");
					c = s.next().charAt(0);
					as = null;
					if(c == 'k'){
						do{
							System.out.println("Anna asiakas id : ");
							as = (Asiakas) dao.haeEntity(Asiakas.class, s.nextInt());
						}while(as == null);
					}
					li = new Lippu(as, pa, na);
					System.out.println("Tallennetaan lippua...");
					dao.tallenna(li);
					System.out.println("Tallennus onnistui!");
					break;
				case 2:
					System.out.println("Anna lippu id : ");
					li = (Lippu) dao.haeEntity(Lippu.class, s.nextInt());
					if(li==null)
						System.out.println("Lipun hakeminen ep�onnistui!");
					else
						System.out.println("Lipun haku onnistui!");
					break;
				case 3:
					if(li != null){
						System.out.println("Suoritetaan lipun poistoa...");
						dao.deleteEntity(li);
						li = null;
						System.out.println("Lippu poistettu onnistuneesti");
					}else{
						System.out.println("Ei lippua k�sittelyss� ei voida poistaa!");
					}
					break;
			}
		}while(smenu != 5);

	}

	private static void n�yt�sMenu() {
		N�yt�s na = null;
		Elokuva el = null;
		Sali sa = null;
		String snimi;
		int smenu = 0;
		char c;
		do{
			snimi = na == null ? "Ei N�yt�st� k�sittelyss�" : "K�sitelt�v� n�yt�s : "
			+ na.toString();

			System.out.println(snimi);
			System.out.println("1.Luo uusi N�yt�s"
			+ "\n2.Hae N�yt�s\n3.Poista N�yt�s\n4.P�ivit� N�yt�st�\n5.<-Takaisin");
			smenu = s.nextInt();
			switch(smenu){
				case 1:
					do{
						s.nextLine();
						System.out.println("Haetaanko tietokannasta Sali(k/e)? ");
						c = s.next().charAt(0);
						if(c == 'k'){
							System.out.println("Anna Salin id : ");
							int id = s.nextInt();
							sa = (Sali)dao.haeEntity(Sali.class, id);
						}else if(c == 'e'){
							sa = luoSali();
						}
						if(sa == null)
							System.out.println("Virhe salia ei viel� m��ritelty!");
					}while(sa == null);

					do{
						s.nextLine();
						System.out.println("Haetaanko tietokannasta Elokuva(k/e)? ");
						c = s.next().charAt(0);
						if(c == 'k'){
							System.out.println("Anna Elokuvan id : ");
							int id = s.nextInt();
							el = (Elokuva)dao.haeEntity(Elokuva.class, id);
						}else if(c == 'e'){
							el = luoElokuva();
						}
						if(el == null)
							System.out.println("Virhe elokuvaa ei viel� m��ritelty!");
					}while(el == null);
					System.out.println("Anna n�yt�ksen hinta : ");
					float hinta = s.nextFloat();
					na = new N�yt�s(new Timestamp(System.currentTimeMillis()),hinta, sa, el);
					System.out.println("Tallennetaan n�yt�st�...");
					if(dao.tallenna(na)){
						System.out.println("Tallennus onnistui!");
					}else{
						System.out.println("Tallennus ep�onnistui!");
					}
					break;
				case 2:
					System.out.println("Anna haettavan n�yt�ksen id : ");
					na = (N�yt�s) dao.haeEntity(N�yt�s.class, s.nextInt());
					if(na == null)
						System.out.println("Haku ep�onnistui!");
					break;
				case 3:
					if(na==null)
						System.out.println("Ei n�yt�st� k�sittelyss�!\nEi voida suorittaa poisto-operaatiota");
					else{
						if(dao.deleteEntity(na)){
							System.out.println("Poisto onnistui!");
							na = null;
						}else{
							System.out.println("Poisto ep�onnistui");
						}
					}
					break;
				case 4:
					if(na == null){
						System.out.println("N�yt�st� ei ole k�sittelyss� ei voida p�ivitt�� mit��n!");
						break;
					}
					System.out.println("Muokataanko Salia(k/e)? ");
					c = s.next().charAt(0);
					if(c == 'k'){
						System.out.println("Salien p�ivitys ei viel� tuettu :)");
					}
					if(na.getElokuva() != null){
						System.out.println("Muokataanko Elokuvaa(k/e)? ");
						c = s.next().charAt(0);
						if(c == 'k'){
							s.nextLine();
							System.out.println("Anna Elokuvan uusi nimi: ");
							na.getElokuva().setNimi(s.nextLine());
							System.out.println("Anna Elokuvan uusi arvosana: ");
							na.getElokuva().setArvosana(s.nextFloat());
						}
					}
					dao.flushEntity();
					System.out.println("P�ivitetty");
					break;
			}
		}while(smenu!=5);
		s.nextLine();
	}

	static void saliMenu(){
		Sali sa = null;
		String snimi;
		int smenu = 0;
		char c;
		do{
			snimi = sa == null ? "Ei salia k�sittelyss�" : "K�sitelt�v� sali : "
			+ sa.getSaliID() + "\nNimi : " + sa.getNimi();

			System.out.println(snimi);
			System.out.println("1.Luo uusi Sali"
			+ "\n2.Hae Sali\n3.Poista Sali\n4.P�ivit� Salia\n5.<-Takaisin");
			smenu = s.nextInt();
			switch(smenu){
				case 1:
					sa = luoSali();
					System.out.println("Tallennetaan Salia...");
					if(dao.tallenna(sa)){
						System.out.println("Tallennus onnistui!");
					}else{
						System.out.println("Tallennus ep�onnistui!");
						sa = null;
					}
					break;
				case 2:
					System.out.println("Anna haettavan salin id : ");
					sa = (Sali) dao.haeEntity(Sali.class, s.nextInt());
					if(sa == null)
						System.out.println("Haku ep�onnistui!");
					break;
				case 3:
					if(sa==null)
						System.out.println("Ei elokuvaa salia!\nEi voida suorittaa poisto-operaatiota");
					else{
						if(dao.deleteEntity(sa)){
							System.out.println("Poisto onnistui!");
							sa = null;
						}else{
							System.out.println("Poisto ep�onnistui");
						}
					}
					break;
				case 4:
					if(sa==null)
						System.out.println("Ei elokuvaa salia!\nEi voida suorittaa p�ivitys-operaatiota");
					else{
						do{
							System.out.println("Luodaanko saliin paikka(k/e)?");
							s.nextLine();
							c = s.next().charAt(0);
							if(c == 'k'){
								System.out.println("Anna paikan paikka numero : ");
								int pnmr = s.nextInt();
								System.out.println("Anna paikan rivi : ");
								int rivi = s.nextInt();
								Paikka pa = new Paikka(rivi, pnmr, sa);
								sa.lisaaPaikka(pa);
							}
						}while(c!='e');
						System.out.println("P�ivitet��nk� nime�(k/e)?");
						s.nextLine();
						c = s.next().charAt(0);
						if(c=='k'){
							System.out.println("Anna uusi nimi : ");
							s.nextLine();
							sa.setNimi(s.nextLine());
						}
						System.out.println("Tallennetaan muutoksia...");
						if(dao.tallenna(sa)){
							System.out.println("success!");
						}else{
							System.out.println("fail!");
						}
					}
					break;
			}
		}while(smenu!=5);
		s.nextLine();
	}

	static void movieMenu(){
		Elokuva el = null;
		String enimi;
		int smenu=0;
		char c;
		do{
			enimi = el == null ? "Ei elokuvaa k�sittelyss�" : "K�sitelt�v� elokuva : "
			+ el.getNimi() + "\nid : " + el.getElokuvaID();

			System.out.println(enimi);
			System.out.println("1.Luo uusi Elokuva"
			+ "\n2.Hae Elokuva\n3.Poista Elokuva\n4.P�ivit� Elokuvaa\n5.<-Takaisin");
			smenu = s.nextInt();
			switch(smenu){
				case 1:
					s.nextLine();
					System.out.println("Anna Elokuvan nimi : ");
					String nimi = s.nextLine();
					System.out.println("Anna Elokuvan arvosana : ");
					float arv = s.nextFloat();
					el = new Elokuva(nimi, arv, new Date(System.currentTimeMillis()));
					System.out.println("Tallennetaan Elokuvaa...");
					if(dao.tallenna(el)){
						System.out.println("Tallennus onnistui!");
					}else{
						System.out.println("Tallennus ep�onnistui!");
						el = null;
					}
					break;
				case 2:
					System.out.println("Anna haettavan elokuvan id : ");
					el = (Elokuva) dao.haeEntity(Elokuva.class, s.nextInt());
					if(el == null)
						System.out.println("Haku ep�onnistui!");
					break;
				case 3:
					if(el==null)
						System.out.println("Ei elokuvaa k�sitelt�v�n�!\nEi voida suorittaa poisto-operaatiota");
					else{
						if(dao.deleteEntity(el)){
							System.out.println("Poisto onnistui!");
							el = null;
						}else{
							System.out.println("Poisto ep�onnistui");
						}
					}
					break;
				case 4:
					if(el==null)
						System.out.println("Ei elokuvaa k�sitelt�v�n�!\nEi voida suorittaa p�ivitys-operaatiota");
					else{
						s.nextLine();
						System.out.println("Anna Elokuvan nimi : ");
						nimi = s.nextLine();
						System.out.println("Anna Elokuvan arvosana : ");
						arv = s.nextFloat();
						System.out.println("Tallennetaanko muutokset (k/e)?");
						s.nextLine();
						c = s.next().charAt(0);
						if(c == 'k'){
							System.out.println("Tallennetaan muutoksia...");
							el.setNimi(nimi);
							el.setArvosana(arv);

							if(dao.tallenna(el)){
								System.out.println("success!");
							}else{
								System.out.println("fail!");
							}
						} else {
							System.out.println("P�ivitys peruttu");
							el = null;
							continue;
						}
					}
					break;

			}
		}while(smenu!=5);
		s.nextLine();
	}

	static void asiakasMenu() {
		Asiakas as = null;
		String animi, asukunimi, aemail;
		int amenu = 0;
		char c;
		do{
			animi = as == null ? "Ei asiakasta k�sittelyss�" : "K�sitelt�v� asiakas : " + as.getAsiakasID()
			+ "\nNimi : " + as.getEtunimi() + " " + as.getSukunimi() + "\nEmail : " + as.getEmail();

			System.out.println(animi);
			System.out.println("1.Luo uusi Asiakas"
							+ "\n2.Hae asiakas"
							+ "\n3.Poista asiakkaan"
							+ "\n4.P�ivit� asiakasta"
							+ "\n5.<-Takaisin");

			amenu = s.nextInt();
			switch(amenu){
				case 1:  // LUO
					s.nextLine();

					System.out.println("Anna nimi: ");
					animi = s.nextLine();
					System.out.println("Anna sukunimi: ");
					asukunimi = s.nextLine();
					System.out.println("Anna email: ");
					aemail = s.nextLine();

					if (tarkistaAsiakas(aemail)) {
						as = new Asiakas(animi, asukunimi, aemail);
					} else {
						System.out.println("Email on jo k�yt�ss�!");
						as = null;
						continue;
					}

					if(dao.tallenna(as)){
						System.out.println("Tallennus onnistui!");
					}else{
						System.out.println("Tallennus ep�onnistui!");
						as = null;
					}
					break;
				case 2: // HAE
					System.out.println("Anna haettavan asiakkaan id : ");

					as = (Asiakas) dao.haeEntity(Asiakas.class, s.nextInt());
					if(as == null) {
						System.out.println("Haku ep�onnistui!");
					}
					break;
				case 3:  // POISTA
					if(as==null)
						System.out.println("Ei asiakasta!\nEi voida suorittaa poisto-operaatiota");
					else{
						if(dao.deleteEntity(as)){
							System.out.println("Poisto onnistui!");
							as = null;
						}else{
							System.out.println("Poisto ep�onnistui");
						}
					}
					break;
				case 4:  // P�IVIT�
					if(as==null)
						System.out.println("Ei asiakasta k�sitelt�v�n�!\nEi voida suorittaa p�ivitys-operaatiota");
					else{
						s.nextLine();
						System.out.println("Anna nimi: ");
						animi = s.nextLine();
						System.out.println("Anna sukunimi: ");
						asukunimi = s.nextLine();
						System.out.println("Anna email: ");
						aemail = s.nextLine();
						//s.nextLine();
						System.out.println("Tallennetaanko muutokset(k/e)?");
						c = s.next().charAt(0);
						if(c == 'k'){
							System.out.println("Tallennetaan muutoksia...");

							if(!as.getEmail().equals(aemail)){
								if (!tarkistaAsiakas(aemail)) {
									System.out.println("ERROR Email on jo k�yt�ss�!\nPalautetaan Asiakkaan tiedot...");
									continue;
								}
								as.setEmail(aemail);
							}

							as.setEtunimi(animi);
							as.setSukunimi(asukunimi);

							if(dao.tallenna(as)){
								System.out.println("success!");
							}else{
								System.out.println("fail!");
							}
						} else {
							System.out.println("P�ivitys peruttu");
							continue;
						}
					}
					break;
			}
		}while(amenu!=5);
		s.nextLine();
	}

	private static boolean tarkistaAsiakas(String email){
		if (email == null || email.equals("")) {
			System.out.println("S�hk�posti on pakollinen");
			return false;
		}
		if (!dao.checkCustomerByEmail(email)) {
			return false;
		}
		return true;
	}

	private static Sali luoSali(){
		Sali sa;
		s.nextLine();
		System.out.println("Anna Salin nimi : ");
		String nimi = s.nextLine();
		sa = new Sali(nimi);
		System.out.println("Anna salin leveys : ");
		int pnmr = s.nextInt();
		System.out.println("Anna salin pituus : ");
		int rivi = s.nextInt();
		sa.getPaikat().clear();
		for(int i = 1; i<(rivi+1) ; i++){
			for(int j = 1; j<(pnmr+1);j++){
				sa.lisaaPaikka(new Paikka(i, j, sa));
			}
		}
		return sa;
	}

	private static Elokuva luoElokuva(){
		s.nextLine();
		System.out.println("Anna Elokuvan nimi : ");
		String nimi = s.nextLine();
		System.out.println("Anna Elokuvan arvosana : ");
		float arv = s.nextFloat();
		return new Elokuva(nimi, arv, new Date(System.currentTimeMillis()));
	}
}