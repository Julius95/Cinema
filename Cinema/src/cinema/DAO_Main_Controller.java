package cinema;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.sql.Date;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import entity.Asiakas;
import entity.Elokuva;
import entity.EntityObject;
import entity.Lippu;
import entity.N�yt�s;
import entity.Paikka;
import entity.Sali;

@SuppressWarnings("unused")
public class DAO_Main_Controller {

	private static int MAX_SALIT;
	private static int RIVIN_PITUUS = 2;

	private EntityManagerFactory emf;
	public static EntityManager em;
	Random random = new Random();

	public DAO_Main_Controller(String dbName){
		emf = Persistence.createEntityManagerFactory(dbName);
		em = emf.createEntityManager();
		System.out.println("Connected to db " + dbName + " successfully!");
	}

	//This function creates some data for our db tables
	public void createDummyData(){
		Asiakas a1 = new Asiakas("Joona", "Kivinen", "joo@email.com");
		Asiakas a2 = new Asiakas("Olavi", "Granlund", "OG@email.com");

		Sali s1 = new Sali("Supper");
		//Rivi r1 = new Rivi(6, s1);
		Paikka p1 = new Paikka(1,1,s1);
		Paikka p2 = new Paikka(1,2,s1);
		Paikka p3 = new Paikka(1,3,s1);
		Paikka p4 = new Paikka(1,4,s1);
		Paikka p5 = new Paikka(2,1,s1);
		Paikka p51 = new Paikka(2,2,s1);
		Paikka p6 = new Paikka(2,3,s1);
		Paikka p7 = new Paikka(2,4,s1);
		Paikka p8 = new Paikka(3,1,s1);
		Paikka p9 = new Paikka(3,2,s1);
		Paikka p10 = new Paikka(3,3,s1);
		Paikka p11 = new Paikka(3,4,s1);

		Elokuva e1 = new Elokuva("Ajokki Leffa");
		Elokuva e2 = new Elokuva("Lehdet", (float) 3.2, new Date(System.currentTimeMillis()));

		Timestamp t = new Timestamp(System.currentTimeMillis());
		N�yt�s n1 = new N�yt�s(t, 12.90f,s1, e2);
		N�yt�s n2 = new N�yt�s(t, 15.60f,s1, e1);

		Lippu l1 = new Lippu(a1, p1, n1);
		Lippu l2 = new Lippu(p2, n1);
		em.getTransaction().begin();
		em.persist(a1);
		em.persist(a2);
		//em.persist(p1);
		//em.persist(p2);
		//em.persist(e1);
		//em.persist(e2);
		em.persist(n1);
		em.persist(n2);
		em.persist(l1);
		em.persist(l2);
		em.getTransaction().commit();
	}


	public void createHalls() {
		System.out.println("Creating cinema halls, please stand by.");
		Sali sali;
		em.getTransaction().begin();

		for (int i = 1; i <= MAX_SALIT; i++) {
			int maxRivit = random.nextInt(7 - 5 + 1) + 5;
			String nimi = "Sali " + i;
			sali = new Sali(nimi); // 5 - 10 rivi�

			for (int riviNmr = 1; riviNmr <= maxRivit; riviNmr++) {
				for (int paikkaNmr = 1; paikkaNmr <= RIVIN_PITUUS; paikkaNmr++) {
					Paikka paikka = new Paikka(riviNmr, paikkaNmr, sali);
				}
				RIVIN_PITUUS +=2;
			}
			em.persist(sali);
			sali = null;
		}
		em.getTransaction().commit();
		System.out.println("Cinema halls created. You may proceed.");
	}

	public boolean tallenna(EntityObject ie){
		try{
			DAO_Main_Controller.em.getTransaction().begin();
			ie.persist();
			DAO_Main_Controller.em.getTransaction().commit();
		}catch(Exception e){
			System.out.println("Error " + e);
			return false;
		}
		return true;
	}

	public EntityObject haeEntity(Class luokka, int id){
		EntityObject ie = null;
		try{
			ie = (EntityObject) em.find(luokka, id);
		}catch(Exception e){
			System.out.println("Error " + e);
		}
		//ie = (IF_Entity<?>) ie.getEntity(id);
		return ie;
	}

	public void flushEntity(){
		try{
			DAO_Main_Controller.em.getTransaction().begin();
			em.flush();
			DAO_Main_Controller.em.getTransaction().commit();
		}catch(Exception e){
			System.out.println("Error " + e);
		}
	}

	public boolean deleteEntity(EntityObject ie){
		if(!DAO_Main_Controller.em.contains(ie)){
			System.out.println("object is not persistent -> cannot remove it");
			return false;
		}

		try{
			DAO_Main_Controller.em.getTransaction().begin();
			ie.chainDelete();
			ie.delete();
			DAO_Main_Controller.em.getTransaction().commit();
		}catch(Exception e){
			System.out.println("Error " + e);
			return false;
		}
		return true;
	}



	public boolean checkCustomerByEmail(String email) {
		System.out.println("ETSIT��N EMAIL: "+email);
		Query kysely = em.createQuery("SELECT p.etunimi FROM Asiakas p WHERE p.email= :email");
		kysely.setParameter("email", email);
		List result = kysely.getResultList();

		return (result.isEmpty()) ? true : false;
	}


	public List<Integer> haeVapaatLiput(int n�yt�sId){
		Query query = em.createNativeQuery("SELECT PaikkaID FROM VapaatLiput WHERE NAYTOSID = ?");
		query.setParameter(1, n�yt�sId);

		List<Integer> is = query.getResultList();
		if(is.isEmpty()){
			return new ArrayList<Integer>();
		}

		return is;
	}

	//select `N�yt�s`.`NAYTOSID` AS `NAYTOSID`,`Paikka`.`PAIKKAID` AS `PaikkaID` from (`Paikka` join `N�yt�s` on((`Paikka`.`SaliID` = `N�yt�s`.`SaliID`))) where (not(`N�yt�s`.`NAYTOSID` in (select `Lippu`.`N�yt�sID` from `Lippu` where (`Paikka`.`PAIKKAID` = `Lippu`.`PaikkaID`))))

}
